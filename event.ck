// generic Event

// define class X
class Conductor
{
    Event e;
    int beats;
    
    fun Conductor @ construct() 
    {
        0 => beats;
        return this;
    }

    fun void loop() 
    {
        while ( true ) 
        {
            beats + 1 => beats;
            e.broadcast();
            
            1::second => now;
        }
    }
}

Conductor conductor;
conductor.construct(); 

class NoteCmd 
{
    int note;
    int velocity;
    int startBeat;
    int durationBeats;   
}


class Player
{
    // current time in beats
    int beats;
    // (boolean) is the player note on?
    int noteIsOn;
    // which beat do I send note off?
    int offBeat;
    // my notes
    NoteCmd @ sequence[];
    SinOsc s;
     
    fun Player @ construct() 
    {
        -1 => beats;
        0 => noteIsOn;
        -1 => offBeat;
        s => dac;
        0.6 => s.gain;
        220 => s.freq;
         
        return this;
    }
        
    // function to spork 
    // returns beat when terminated
    fun int playSequence( Event e)
    {
        while (sequence.cap() > 0)
        {
            // wait 
            e => now;
                
            <<< "conductor.beats = ", conductor.beats >>>;

            // if we are on a start beat for next note, start note
            if ( conductor.beats == sequence[0].startBeat)  {
                0.6 => s.gain;
                <<< sequence.cap() >>>;
                sequence[0].durationBeats::second => now;
                <<< "played on beat ", conductor.beats >>>;
                removeHeadFromSequence();
            }
            0 => s.gain;
                
        }
        <<< "End sequence." >>>;
        return conductor.beats;
    }
    
    // remove first NoteCmd from sequence
    // return number of NoteCmds in new sequence
    fun int removeHeadFromSequence()
    {
        <<< "Removing head from sequence, cap=", sequence.cap() >>>;
        NoteCmd newSequence[sequence.cap()-1];
        for (1 => int i; i<sequence.cap(); i++) {
            sequence[i] @=> newSequence[i-1];
        }
        newSequence @=> sequence; 
    }
   
    fun int setSequence(NoteCmd newNotes[])
    {
        newNotes @=> sequence;
        0 => beats;
    }
}

// set up notes to play
int raw[][];
NoteCmd sequence[];
[[440, 70, 1, 2], [440, 70, 5, 1]] @=> raw;

// set up player
Player player1;
player1.construct();
[createNote(raw[0]), createNote(raw[1]) ] @=> player1.sequence;


fun NoteCmd @ createNote(int values[]) {
    NoteCmd @ nc;
    if (values.cap() == 4)  {
        new NoteCmd @=> nc;
        values[0] => nc.note;
        values[1] => nc.velocity;
        values[2] => nc.startBeat;
        values[3] => nc.durationBeats;
    } else {
        <<< "Error: expected array with 4 values" >>>;
    }
    return nc;
}


// spork!
//spork ~ foo( e, 220 );
spork ~ player1.playSequence( conductor.e );

1::second => now;

conductor.loop(); 


